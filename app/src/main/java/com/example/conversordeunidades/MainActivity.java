package com.example.conversordeunidades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] unidades = new String[]{
                "T",
                "Wb/cm^2",
                "Wb/m^2",
                "Mx/cm^2",
                "Mx/m^2",
                "G"
        };

        Spinner sp = this.findViewById(R.id.spinner);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, unidades);
        sp.setAdapter(spinnerAdapter);

        Spinner spb = this.findViewById(R.id.spinner2);
        ArrayAdapter<String> spinnerAdapterB = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, unidades);
        spb.setAdapter(spinnerAdapterB);

        spb.getSelectedItemPosition();
    }
}
